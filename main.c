
#include "raylib.h"
#include <math.h> 

#ifndef M_TWO_PI
#define M_TWO_PI 6.2831853071795864769252867665590f
#endif

#define TILE 24 
#define UIH 13

// PALETTES ( foreground / background) 
static const Color colors[] = { 
	(Color){ 0, 0, 0, 255 }, (Color){ 255, 255, 255, 255 },
	(Color){ 255, 255, 255, 255 }, (Color){ 0, 0, 0, 255 },
	(Color){ 139, 172, 15, 255 }, (Color){ 15, 56, 15, 255 }, 
	(Color){ 255, 26, 255, 255 }, (Color){ 0, 0, 0, 255 },
	
	(Color){ 0, 0, 0, 255 }, (Color){ 255, 85, 85, 255 },
	(Color){ 255, 85, 255, 255 }, (Color){  85, 255, 255, 255 },
	(Color){ 103, 114, 169, 255 }, (Color){ 58, 50, 119, 255 },	
	(Color){ 178, 34, 34, 255 }, (Color){ 22, 27, 29, 255 },

	(Color){ 114, 222, 194, 255 }, (Color){ 34, 34, 34, 255 },
	(Color){ 34, 34, 34, 255 }, (Color){ 191, 186, 172, 255 },
	(Color){ 245, 240, 236, 255 }, (Color){ 169, 93, 80, 255 },
	(Color){ 162, 120, 80, 255 }, (Color){ 35, 35, 35, 255 }
};
#define MAX_PALETTES 12

typedef struct Theming {
	Color fg;
	Color bg;
	int index;
} Theming;

/* MEMO FRAMES
0		default
1-2		watch back
3 		from watch back to def
4		from watch back to turned
5		idle down
6-8		angry loop
9-10	prealarm
11-14	alarm loop
15-16	from def to walk (don't move on 15)
17-24 	walk cycle (mov 1px)
*/

#define STATE_DEFAULT 0
#define STATE_WATCH_BACK 1
#define STATE_WATCH_BACK_TO_DEF 2
#define STATE_WATCH_BACK_TO_TURN 3 
#define STATE_IDLE_DOWN 4
#define STATE_ANGRY_LOOP 5
#define STATE_PREALARM 6
#define STATE_ALARM 7
#define STATE_TO_WALK 8
#define STATE_WALKING 9
#define BOUNDARY 10 
#define STATE_WATCH_BACK_TO_TURN_AND_WALK 11

static const int firstFrame[] = { 0, 1, 3, 4, 5, 6, 9, 11, 15, 17, 24, 4 }; 

typedef struct AnimData {
	int x;
	int y;
	int frame;
	int state;
	int counter;
	int flip;
} AnimData;

AnimData stateMachine( AnimData anim, int width ){
	switch( anim.state ){
		case STATE_DEFAULT: {
			int dice = GetRandomValue(0, 70 );

			if( dice < 1 ){
				anim.state = STATE_WATCH_BACK;
				anim.frame = firstFrame[STATE_WATCH_BACK];;
			}
			else if( dice < 2 ){
				anim.state = STATE_TO_WALK;
				anim.frame = firstFrame[STATE_TO_WALK];
			}
			else if( dice < 5 ){
				anim.state = STATE_IDLE_DOWN;
				anim.frame = firstFrame[STATE_IDLE_DOWN];
				anim.counter = 2;
			} 
		} break;

		case STATE_IDLE_DOWN:
			anim.counter--;
			if( anim.counter == 0 ){
				anim.state = STATE_DEFAULT;
				anim.frame = firstFrame[STATE_DEFAULT];
			}
		break;
		
		case STATE_WATCH_BACK:{
			if( anim.frame <2 ){
				anim.frame++;
			}

			// more away from the center, more it's possible to flip
			int boost = 0;
			if ( anim.flip ){
				boost = (width-anim.x) / 8;
			}else{
				boost = anim.x / 8;
			}

			int dice = GetRandomValue(0, 80 );
			if( dice < 2  ){
				anim.state = STATE_WATCH_BACK_TO_DEF;
				anim.frame = firstFrame[STATE_WATCH_BACK_TO_DEF];
			}
			else if( dice < 2 + boost){
				anim.state = STATE_WATCH_BACK_TO_TURN;
				anim.frame = firstFrame[STATE_WATCH_BACK_TO_TURN];
			}
		}break;			

		case STATE_WATCH_BACK_TO_TURN :
			anim.flip = !anim.flip;
			anim.state = STATE_DEFAULT;
			anim.frame = firstFrame[STATE_DEFAULT];
		break;
		
		case STATE_WATCH_BACK_TO_TURN_AND_WALK :
			anim.flip = !anim.flip;
			anim.state = STATE_TO_WALK;
			anim.frame = firstFrame[STATE_TO_WALK];
		break;
		
		case STATE_TO_WALK:
			if( anim.frame <16 ){
				anim.frame++;
			}else{
				if( anim.flip ){
					anim.x--;
				}else{
					anim.x++;
				}
				anim.state = STATE_WALKING;
				anim.frame = firstFrame[STATE_WALKING];
			}
		break;

		case STATE_WALKING:
			if( anim.frame < 24 ){
				anim.frame++;
				if( anim.flip ){
					anim.x--;
				}else{
					anim.x++;
				}
				if( anim.x > width-TILE || anim.x < 0 ) {
					anim.state = STATE_WATCH_BACK_TO_TURN_AND_WALK;
					anim.frame = firstFrame[STATE_WATCH_BACK_TO_TURN_AND_WALK];
				}
			}else{
				if( GetRandomValue(0, 4) > 0 ){
					anim.state = STATE_DEFAULT;
					anim.frame = firstFrame[STATE_DEFAULT];
				}else{ // keep walking
					anim.state = STATE_WALKING;
					anim.frame = firstFrame[STATE_WALKING];
				}
			}
		break;		
		
		case STATE_ANGRY_LOOP:
			if( anim.counter != 0 ){
				if( anim.frame < 8 ){
					anim.frame++;
				}else{
					anim.frame = 6;
				}
				anim.counter--;
			}else{
				anim.state = STATE_DEFAULT;
				anim.frame = firstFrame[STATE_DEFAULT];
			}
		break;

		case STATE_PREALARM:
			if( anim.frame == firstFrame[STATE_PREALARM] ){
				anim.frame++;
				anim.counter = 12; // alarm hold
			}else{
				anim.counter--;
				if( anim.counter == 0 ){
					anim.counter = 11; // number of animation loops
					anim.state = STATE_ALARM;
					anim.frame = firstFrame[STATE_ALARM];
				}
			}
		break;

		case STATE_ALARM:
			anim.frame++;
			if( anim.frame == firstFrame[STATE_TO_WALK] ){
				anim.frame = firstFrame[STATE_ALARM];
				anim.counter--;
			}
			if( anim.counter == 0 ){
				anim.state = STATE_DEFAULT;
				anim.frame = firstFrame[STATE_DEFAULT];
			}
		break;
		
		default: 
			anim.state = STATE_DEFAULT;
			anim.frame = firstFrame[STATE_DEFAULT];
		break;
	}

	return anim;
}

#define SFX_BUTTON 0 
#define SFX_ALARM 1 
#define SFX_ANGRY 2 
#define SFX_VOLUME 3
#define SFX_SIZE 4

void setSoundsVolume( Sound * sfx, int volume ){
	// db to linear amp 
	float amp = powf(10.0f, 0.05f * (float)volume);

	for( int i=0; i<SFX_SIZE; ++i){
		SetSoundVolume( sfx[i], amp );
	}
}

#define TIMER_STOPPED 0 
#define TIMER_STARTED 1
#define TIMER_ALARM 2 
#define TIMER_TO_START 3
#define TIMER_TO_STOP 4

typedef struct Timer {
	int minutes;
	int state;
	double start;
} Timer;

typedef struct UIData {
	Rectangle source;
	Rectangle dest;
	Vector2 offset;
	int lx;
	int lPressed;
	int rx; 
	int rPressed;
	int trasparent;
	int pixelsize;
	int width;
	int height;
} UIData;

#define TEX_BIRD 0
#define TEX_CLOCK 1
#define TEX_ARROWS 2
#define TEX_SIZE 3

#define STORAGE_MINUTES 0 
#define STORAGE_THEME 1 
#define STORAGE_VOLUME 2
#define STORAGE_TRASPARENT 3

// ------------------------- INITIALIZATION -------------------------------------
int main(void)
{
	UIData ui = (UIData){ 0 };

	ui.width = 80;
	ui.height = 40;
	ui.pixelsize = 6;
	int screenWidth = ui.width*ui.pixelsize;
	int screenHeight = ui.height*ui.pixelsize;

	SetTraceLogLevel( LOG_ERROR );    
	SetConfigFlags(	FLAG_VSYNC_HINT | FLAG_WINDOW_RESIZABLE 
	                                | FLAG_WINDOW_TRANSPARENT );
	InitWindow(screenWidth, screenHeight, "birdoro");
	SetTargetFPS(60);
	InitAudioDevice(); 
	ChangeDirectory(GetApplicationDirectory());

	// The fbo's height is flipped, because OpenGL     
	ui.source = (Rectangle){ 0.0f, 0.0f, (float)ui.width, -(float)ui.height };
	ui.dest = (Rectangle){ 0, 0, screenWidth, screenHeight };
	ui.offset = (Vector2){ 0.0f, 0.0f };

	ui.lx = ui.width / 3;
	ui.rx = ui.lx * 2;
	ui.lPressed = 0;
	ui.rPressed = 0;

	ui.trasparent = LoadStorageValue( STORAGE_TRASPARENT );
	if( ui.trasparent <0 || ui.trasparent >1 ){ ui.trasparent = 0; }

	Theming theme = (Theming){ 0 };
	theme.index = LoadStorageValue( STORAGE_THEME );
	if( theme.index <0 || theme.index >= MAX_PALETTES ){
		theme.index = 0;
	}
	theme.fg = colors[theme.index*2];
	theme.bg = colors[theme.index*2 + 1];

	Timer timer = (Timer){ 0 };
	timer.minutes = LoadStorageValue( STORAGE_MINUTES );
	if( timer.minutes <= 0 ){ timer.minutes = 25; }
	timer.state = TIMER_STOPPED;
	timer.start = 0.0;

	AnimData anim = (AnimData){ 	
		.x = 8, 
		.y = 4,
		.frame = 0, 
		.state = STATE_DEFAULT, 
		.counter = 0, 
		.flip = 0 
	};

	RenderTexture2D fbo = LoadRenderTexture(ui.width, ui.height); 

	Texture2D tex [ TEX_SIZE ];
	tex[ TEX_BIRD ] = LoadTexture( "tex/birb_24x24.png" );
	tex[ TEX_CLOCK] = LoadTexture( "tex/clock.png" );
	tex[ TEX_ARROWS ] = LoadTexture( "tex/arrows.png" );

	Sound sfx [SFX_SIZE];
	sfx[ SFX_BUTTON ] = LoadSound( "sfx/button.ogg" );
	sfx[ SFX_ALARM ] = LoadSound( "sfx/alarm_seq.ogg" );
	sfx[ SFX_ANGRY ] = LoadSound( "sfx/angry.ogg" );
	sfx[ SFX_VOLUME ] = LoadSound( "sfx/volume.ogg" );

	int volume = LoadStorageValue( STORAGE_VOLUME );
	if( volume > 0 ){ volume = 0; }
	if( volume < -36 ){ volume = -36; }
	setSoundsVolume( sfx, volume );


	// ----------------------------- GAME LOOP ----------------------------------
	int c = 0;		
	int longpress = 0;	

	while (!WindowShouldClose())    // Detect window close button or ESC key
	{
		// update ---------------------------------------------------------------
		if( c == 0 ){
			anim = stateMachine( anim, ui.width ); 
		}
		c++;
		if( c == 5 ){ c = 0; } // 12 fps logic 

		if( timer.state == TIMER_STARTED && 
			(GetTime()-timer.start) > (60.0 * (double)timer.minutes) ){
			anim.state = STATE_PREALARM;
			anim.frame = firstFrame[STATE_PREALARM];
			timer.state = TIMER_ALARM;

			PlaySound( sfx[SFX_ALARM] );
			}
		// 4.8s is alarm duration
		if( timer.state == TIMER_ALARM && 
			(GetTime()-timer.start) > 4.8 + (60.0 * (double)timer.minutes) ){
			timer.state = TIMER_STOPPED;
		}

		// 60fps interactions --------------------------------------------------- 
		if( IsWindowResized() ){
			int sw = GetRenderWidth();
			int sh = GetRenderHeight();
			int nps0 = sw / ui.width;
			int nps1 = sh / ui.height; 
			if( nps0 < nps1 ){
				ui.pixelsize = nps0;
			}else{
				ui.pixelsize = nps1;
			}
			ui.offset.x = -(sw - ui.width*ui.pixelsize)/2;
			ui.offset.y = -(sh - ui.height*ui.pixelsize)/2;
			ui.dest.width = ui.width * ui.pixelsize;
			ui.dest.height = ui.height * ui.pixelsize;
		}

		if( IsMouseButtonPressed( MOUSE_BUTTON_LEFT )){
			int mx = GetMouseX();
			int my = GetMouseY();
			int mxp = (mx + ui.offset.x) / ui.pixelsize;
			int myp = (my + ui.offset.y) / ui.pixelsize;

			int x = anim.x+4;
			int y = anim.y+3;
			int x2 = x + TILE-8;
			int y2 = y + TILE-3;
			if( mxp > x && mxp < x2 && myp > y && myp < y2 ){
				anim.state = STATE_ANGRY_LOOP;
				anim.frame = firstFrame[STATE_ANGRY_LOOP];
				anim.counter = 3*3;

				float dist = GetRandomValue(10*ui.pixelsize, 40*ui.pixelsize);
				float angle =  GetRandomValue(0, M_TWO_PI);
				int ox = mx + cos(angle) * dist;
				int oy = my + sin(angle) * dist;
				SetMousePosition( ox, oy );
				PlaySound( sfx[SFX_ANGRY] );
			}

			if( myp > ui.height - UIH && myp < ui.height ){
				if( timer.state == TIMER_STOPPED ){
					if( mxp < ui.lx ){
						ui.lPressed = 1;
						timer.minutes--;
						if( timer.minutes < 1 ){ timer.minutes = 1; }
					}else if( mxp > ui.rx ){
						ui.rPressed = 1;
						timer.minutes++;
					}else{
						timer.state = TIMER_TO_START;
					}
				}else{
					timer.state = TIMER_TO_STOP;
				}
			}
		}

		if( IsKeyPressed( KEY_ENTER ) || IsKeyPressed( KEY_SPACE )){
			if( timer.state == TIMER_STOPPED ){ 
				timer.state = TIMER_TO_START;
			}else if( timer.state == TIMER_STARTED ){
				timer.state = TIMER_TO_STOP;
			}
		}

		if( timer.state == TIMER_TO_STOP ){
			timer.state = TIMER_STOPPED;
			PlaySound( sfx[SFX_BUTTON] );
		}
		
		if( timer.state == TIMER_TO_START ){
			timer.state = TIMER_STARTED;
			timer.start = GetTime();
			PlaySound( sfx[SFX_BUTTON] );
		}

		if( IsMouseButtonDown( MOUSE_BUTTON_LEFT ) ){
			longpress++;
			if( longpress > 20 && c==0 ){
				if( ui.lPressed ){
					timer.minutes--;
					if( timer.minutes < 1 ){ timer.minutes = 1; }
				}
				if( ui.rPressed ){
					timer.minutes++;
				}
			}
		}

		if( IsMouseButtonReleased( MOUSE_BUTTON_LEFT ) ){
	    	ui.lPressed = 0;
    		ui.rPressed = 0;
    		longpress = 0;
		}

		if( timer.state == TIMER_STOPPED ){			
			int mouseWheel = GetMouseWheelMove();
			if( IsKeyPressed( KEY_DOWN ) || mouseWheel == -1 ){
				timer.minutes--;
				if( timer.minutes < 1 ){ timer.minutes = 1; }
			}else if( IsKeyPressed( KEY_UP ) || mouseWheel == 1 ){
				timer.minutes++;
			}
		}

		if( IsKeyPressed( KEY_ONE ) ){
			theme.index++;
			if( theme.index >= MAX_PALETTES ){
				theme.index = 0;
			}
			theme.fg = colors[theme.index*2];
			theme.bg = colors[theme.index*2 + 1];
		}

		if( IsKeyPressed( KEY_TWO ) ){
			ui.trasparent = !ui.trasparent;
		}
		
		if(  IsKeyPressed( KEY_THREE ) ){
			volume--;
			if( volume < -36 ){
				volume = -36;
			}
			setSoundsVolume( sfx, volume );
			PlaySound( sfx[SFX_VOLUME] );
			
		}else if( IsKeyPressed( KEY_FOUR ) ){
			volume++;
			if( volume > 0 ){
				volume = 0;
			}
			setSoundsVolume( sfx, volume );
			PlaySound( sfx[SFX_VOLUME] );
		}

		// draws the pixels -----------------------------------------------------
		BeginTextureMode(fbo);
			if( ui.trasparent ){
				ClearBackground((Color){ 0, 0, 0, 0 });
			}else{
				ClearBackground(theme.bg);
			}	
            
			Rectangle frameRec;
			if( anim.flip ){
				frameRec = (Rectangle){ TILE*anim.frame+1, 0, -TILE, TILE };
			}else{
 				frameRec = (Rectangle){ TILE*anim.frame, 0, TILE, TILE };
			}
			Vector2 position = { anim.x, anim.y };
			DrawTextureRec(tex[ TEX_BIRD ], frameRec, position, theme.fg);

			int ly = ui.height - UIH;
			DrawLine( 0, ly, ui.width, ly, theme.fg );

			//DrawRectangleLines( 0, 0, ui.width, ui.height, theme.fg );
			switch( timer.state ){
			case TIMER_STOPPED:
				DrawLine( ui.lx, ui.height-UIH, ui.lx, ui.height, theme.fg );
				DrawLine( ui.rx, ui.height-UIH, ui.rx, ui.height, theme.fg );

				DrawText( TextFormat("%i m", timer.minutes), 
				          ui.lx+2, ui.height-UIH+3, 8, theme.fg );
				int bi = 0;
				if( ui.lPressed){ bi = 1; }
				frameRec = (Rectangle){ bi*12, 0, 12, 12 };
				DrawTextureRec( tex[ TEX_ARROWS ], frameRec, 
				                (Vector2){ui.lx-13, ui.height-UIH}, theme.fg );
				bi = 2;
				if( ui.rPressed ){ bi = 3; }
				frameRec = (Rectangle){ bi*12, 0, 12, 12 };
				DrawTextureRec( tex[ TEX_ARROWS ], frameRec, 
				                (Vector2){ui.rx, ui.height-UIH}, theme.fg );			
			break;
			case TIMER_STARTED: 
				DrawTexture( tex[ TEX_CLOCK ], 
				             ui.lx+7, ui.height - UIH+1, theme.fg );
			break;
			
			case TIMER_ALARM:
				if( ((int)(GetTime()*3.0))%2 == 0 ){
					DrawTexture( tex[ TEX_CLOCK ], 
					             ui.lx+7, ui.height - UIH+1, theme.fg );
				}else{
					DrawRectangle( 0, ly, ui.width, UIH+2, theme.fg );
					DrawTexture( tex[ TEX_CLOCK ], 
					             ui.lx+7, ui.height - UIH+1, theme.bg );
				}
			break;
			}

			DrawRectangleLines( 0, 0, ui.width, ui.height, theme.fg );
		EndTextureMode();

		// redraw full scale on window ------------------------------------------
		BeginDrawing();
			if( ui.trasparent ){
				ClearBackground((Color){0,0,0,0});
			}else{
				ClearBackground(theme.fg);
			}

			DrawTexturePro(	fbo.texture, ui.source, ui.dest, 
			                ui.offset, 0.0f, WHITE);
		EndDrawing(); 
	} 

	// -------------------------- QUITTING --------------------------------------

	// Unload things ---------------
	UnloadRenderTexture( fbo );   

	for( int i=0; i<TEX_SIZE; ++i){
		UnloadTexture( tex[i] );
	}

	for( int i=0; i<SFX_SIZE; ++i){
		UnloadSound( sfx[i] );
	}

	// Save states -----------------
	SaveStorageValue(STORAGE_MINUTES, timer.minutes);
	SaveStorageValue(STORAGE_THEME, theme.index);
	SaveStorageValue(STORAGE_VOLUME, volume);
	SaveStorageValue(STORAGE_TRASPARENT, ui.trasparent);

	// Close window and OpenGL context	
	CloseWindow();                  

	return 0;
}
